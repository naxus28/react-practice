//action constants
const ADD_TODO = 'ADD_TODO';
const TOGGLE_TODO = 'TOGGLE_TODO';
const SET_VISIBILITY_FILTER = 'SET_VISIBILITY_FILTER';


//action creators
const addTodo = (text) => {
	return {
		type: ADD_TODO,
		text
	}
}
const toggleTodo = (id) => {
	return {
		type: TOGGLE_TODO,
		id
	}
}
const setVisibilityFilters = (filter) => {
	return{
		type: SET_VISIBILITY_FILTER,
		filter
	}
}

//reducers
let nextId = 0;
const todos = (state=[], action) => {
	switch(action.type){
		
		case 'ADD_TODO':
			return [
				...state,
				{
					id: nextId++,
					text: action.text,
					completed: false
				}
			]
		
		case 'TOGGLE_TODO':
		return state.map(todo=>{
			if(todo.id !== action.id){
				return todo;
			}

			return {
				...todo,
				completed: !todo.completed
			}
		});

		default:
			return state;
	}
}

const visibilityFilters = (state='SHOW_ALL', action) => {
	switch(action.type){
		case 'SET_VISIBILITY_FILTER':
			return action.filter;
		default:
			return state;
	}
}

//combine reducers
 const { combineReducers } = Redux; 

 const todoApp = combineReducers({
 	todos,
 	visibilityFilters
 });

//create store with combined reducers
const { createStore } = Redux;
const store = createStore(todoApp);

/*PRESENTATIONAL COMPONENTS--JUST RENDER THE UI--NO BEHAVIOR SPECIFIED*/
//FilterLink component
const FilterLink = ({
	children,
	filter,
	currentFilter,
	onClick
}) => {
	if(filter === currentFilter){
		return <span>{children}</span>;
	}
	return(
		<a href="#"
			onClick={e => {
				e.preventDefault();
				onClick(filter);
			}
		}>
			{children}
		</a>

	);
}

//function that returns todos accoring to the visibility filter selected
const getVisibleTodos = (
	todos, 
	filter
) => {
	switch(filter){
		case 'SHOW_ALL':
			return todos;

		case 'SHOW_COMPLETED':
			return todos.filter(
				t => t.completed
			);
		case 'SHOW_ACTIVE':
			return todos.filter(
				t => !t.completed
			);
	}
	
}

//individual todo component
const Todo = ({
	completed,
	text,
	onClick
}) => (
	<li onClick={onClick}
		style={{textDecoration: completed? 'line-through': 'none', cursor: 'pointer'}}
	>
		{text}
	</li>
);

//list of todos--loops over all selected todos (using the result of getVisibleTodos) and renders each visible todo
const TodoList = ({
	visibleTodos,
	onTodoClick
}) => (
	<ul>
		{visibleTodos.map(todo =>
			<Todo 
				{...todo} 
				key={todo.id} 
				onClick={()=>
					onTodoClick(todo.id)
				}/>
		)}
	</ul>
);

//renders the input field and button to add todo
//specifies behavior on click
const AddTodo = () => {
	let input;
	return(
		<div>
			<input type="text" ref={ node => {
				input = node;
			}}/>
			<button onClick={() => {
				if(input.value=='')
				{
					alert('Enter a value');
					return;
				}
				else
				{
					store.dispatch(addTodo(input.value));
				}
				
				input.value='';//clears the input after an action is dispatched
			}}>
				Add Todo
			</button>
		</div>
	);
};


//renders the filters that will filter which todos should be visible
const Footer = ({
	currentFilter,
	onFilterClick
}) => (
	<p>
		Show: 
		{' '}
		<FilterLink
			filter='SHOW_ALL'
			currentFilter={currentFilter}
			onClick={onFilterClick}
		>
				All
		</FilterLink>
		{' | '}
		<FilterLink
			filter='SHOW_COMPLETED'
			currentFilter={currentFilter}
			onClick={onFilterClick}
		>
				Completed
		</FilterLink>
		{' | '}
		<FilterLink
			filter='SHOW_ACTIVE'
			currentFilter={currentFilter}
			onClick={onFilterClick}
		>
				Not Completed
		</FilterLink>
	</p>
)


//controller component--renders other components and passes data needed to render each of them
const TodoApp = ({
	todos,
	visibilityFilters
}) => (
	<div>
		<AddTodo />
		<TodoList 
			visibleTodos={
			getVisibleTodos(
				todos,
				visibilityFilters
			)}

			onTodoClick={id =>
				store.dispatch(toggleTodo(id))
			}/>

		<Footer 
			currentFilter={visibilityFilters}
			onFilterClick={filter=>
				store.dispatch(setVisibilityFilters(filter))
			}/>
	</div>
);

//render the controller component and passes the state tree as props
const render = () => {
	ReactDOM.render(
		<TodoApp {...store.getState()}/>,
		document.getElementById('root')
	);
}

//re-render the whole app for every action dispatched
store.subscribe(render);

//initial render of the app before any action is dispatched
render();

/***************************CONSOLE LOGS***************************/
/*
//Initial state
console.log('Initial State');
console.log(store.getState());


//Dispatching action 1
console.log('---------------------');
console.log('Dispatching action 1');
store.dispatch(addTodo('Hello World'));

console.log(store.getState());


//Dispatching action 2
console.log('---------------------');
console.log('Dispatching action 2');
store.dispatch(addTodo('Hello Dudes'));

console.log(store.getState());


//Dispatching action 3
console.log('---------------------');
console.log('Dispatching action 3');
store.dispatch(addTodo('Hello my Friends'));

console.log(store.getState());


//Dispatching action 4
console.log('---------------------');
console.log('Dispatching action 4');
store.dispatch(toggleTodo(3));

console.log(store.getState());


//Dispatching action 5
console.log('---------------------');
console.log('Dispatching action 5');
store.dispatch(setVisibilityFilters('SHOW_COMPLETED'));

console.log(store.getState());


*/








