//action constants
const ADD_TODO = 'ADD_TODO';
const TOGGLE_TODO = 'TOGGLE_TODO';
const SET_VISIBILITY_FILTER = 'SET_VISIBILITY_FILTER';


//action creators
const addTodo = (text) => {
	return {
		type: ADD_TODO,
		text
	}
}
const toggleTodo = (id) => {
	return {
		type: TOGGLE_TODO,
		id
	}
}
const setVisibilityFilters = (filter) => {
	return{
		type: SET_VISIBILITY_FILTER,
		filter
	}
}

//reducers
let nextId = 0;
const todos = (state=[], action) => {
	switch(action.type){
		
		case 'ADD_TODO':
			return [
				...state,
				{
					id: nextId++,
					text: action.text,
					completed: false
				}
			]
		
		case 'TOGGLE_TODO':
		return state.map(todo=>{
			if(todo.id !== action.id){
				return todo;
			}

			return {
				...todo,
				completed: !todo.completed
			}
		});

		default:
			return state;
	}
}

const visibilityFilters = (state='SHOW_ALL', action) => {
	switch(action.type){
		case 'SET_VISIBILITY_FILTER':
			return action.filter;
		default:
			return state;
	}
}

//combine reducers
 const { combineReducers } = Redux; 

 const todoApp = combineReducers({
 	todos,
 	visibilityFilters
 });

//initialize react component
const { Component } = React;

/*PRESENTATIONAL COMPONENTS--JUST RENDER THE UI--NO BEHAVIOR SPECIFIED*/
//FilterLink component
const Link = ({
	children,
	active,
	onClick
}) => {
	if(active){
		return <span>{children}</span>;
	}
	return(
		<a href="#"
			onClick={e => {
				e.preventDefault();
				onClick();
			}
		}>
			{children}
		</a>

	);
}

// class FilterLink extends Component {
// 	componentDidMount() {
// 		const { store } = this.context;
// 		this.unsubscribe = store.subscribe(() =>
// 			this.forceUpdate()
// 		);
// 	}

// 	componentWillUnmount() {
// 		this.unsubscribe();
// 	}

// 	render(){
// 		const props = this.props;
// 		const { store } = this.context;
// 		const state = store.getState();
		
// 		return(
// 			<Link 
// 				active={
// 					props.filter===state.visibilityFilters
// 				}
// 				onClick={}>
// 				{this.props.children}
// 			</Link>

// 		);
// 	}
// }

// FilterLink.contextTypes = {
// 	store: React.PropTypes.object
// }


//takes the state from the connect function
const mapStateToLinkProps = (state, ownProps) => {
	return {
		active: ownProps.filter===state.visibilityFilters
	};
}

//takes the action from the connect function
const mapDispatchToLinkProps = (dispatch, ownProps) => {
	return {
		onClick: () =>{
			return(
				dispatch(setVisibilityFilters(ownProps.filter))
			);
		}
	}
}

//connect method from react-redux lib
const { connect } = ReactRedux;

//merge the two objects returned from the above actions and passes it to the 
//container component TodoList
const FilterLink = connect(
	mapStateToLinkProps,
	mapDispatchToLinkProps
)(Link);

//function that returns todos accoring to the visibility filter selected
const getVisibleTodos = (
	todos, 
	filter
) => {
	switch(filter){
		case 'SHOW_ALL':
			return todos;

		case 'SHOW_COMPLETED':
			return todos.filter(
				t => t.completed
			);
		case 'SHOW_ACTIVE':
			return todos.filter(
				t => !t.completed
			);
	}
	
}

//individual todo component
const Todo = ({
	completed,
	text,
	onClick
}) => (
	<li onClick={onClick}
		style={{textDecoration: completed? 'line-through': 'none', cursor: 'pointer'}}
	>
		{text}
	</li>
);

//list of todos--loops over all selected todos (using the result of getVisibleTodos) and renders each visible todo
const TodoList = ({
	visibleTodos,
	onTodoClick
}) => (
	<ul>
		{visibleTodos.map(todo =>
			<Todo 
				{...todo} 
				key={todo.id} 
				onClick={()=>
					onTodoClick(todo.id)
				}/>
		)}
	</ul>
);

//takes the state from the connect function
const mapStateToProps = (state) => {
	return {
		visibleTodos: getVisibleTodos(
			state.todos,
			state.visibilityFilters
		)
	};
}

//takes the action from the connect function
const mapDispatchToProps = (dispatch) => {
	return {
		onTodoClick: (id) =>{
			return(
				dispatch(toggleTodo(id))
			);
		}
	}
}

//merge the two objects returned from the above actions and passes it to the 
//container component TodoList
const VisibleTodoList = connect(
	mapStateToProps,
	mapDispatchToProps
)(TodoList);

// class VisibleTodoList extends Component{
// 	componentDidMount() {
// 		const { store } = this.context;
// 		this.unsubscribe = store.subscribe(() =>
// 			this.forceUpdate()
// 		);
// 	}

// 	componentWillUnmount() {
// 		this.unsubscribe();
// 	}

// 	render(){
// 		const { store } = this.context;
// 		const state = store.getState();

// 		return(
// 			<TodoList 
// 				visibleTodos={}

// 				onTodoClick={id =>
// 					store.dispatch(toggleTodo(id))
// 				}/>
// 		);
// 	}
// }

// VisibleTodoList.contextTypes = {
// 	store: React.PropTypes.object
// }


//renders the input field and button to add todo
//specifies behavior on click


let AddTodo = ({ dispatch }) => {
	let input;
	return(
		<div>
			<input type="text" ref={ node => {
				input = node;
			}}/>
			<button onClick={() => {
				if(input.value=='')
				{
					alert('Enter a value');
					return;
				}
				else
				{
					dispatch(addTodo(input.value));
				}
				
				input.value='';//clears the input after an action is dispatched
			}}>
				Add Todo
			</button>
		</div>
	);
};

AddTodo = connect()(AddTodo);

//renders the filters that will filter which todos should be visible
const Footer = () => (
	<p>
		Show: 
		{' '}
		<FilterLink
			filter='SHOW_ALL'
		>
				All
		</FilterLink>
		{' | '}
		<FilterLink
			filter='SHOW_COMPLETED'
		>
				Completed
		</FilterLink>
		{' | '}
		<FilterLink
			filter='SHOW_ACTIVE'
		>
				Not Completed
		</FilterLink>
	</p>
)

//create store with combined reducers
const { createStore } = Redux;

const { Provider } = ReactRedux;

// class Provider extends Component{
// 	getChildContext(){
// 		return{
// 			store: this.props.store
// 		}
// 	}
// 	render(){
// 		return this.props.children;
// 	}
// }

// Provider.childContextTypes = {
// 	store: React.PropTypes.object
// }
//controller component--renders other components and passes data needed to render each of them
const TodoApp = () => (
	<div>
		<AddTodo />
		<VisibleTodoList />
		<Footer />
	</div>
);

//render the controller component and passes the state tree as props
ReactDOM.render(
	<Provider store={createStore(todoApp)}>
		<TodoApp />
	</Provider>
	,
	document.getElementById('root')
);

/***************************CONSOLE LOGS***************************/
/*
//Initial state
console.log('Initial State');
console.log(store.getState());


//Dispatching action 1
console.log('---------------------');
console.log('Dispatching action 1');
store.dispatch(addTodo('Hello World'));

console.log(store.getState());


//Dispatching action 2
console.log('---------------------');
console.log('Dispatching action 2');
store.dispatch(addTodo('Hello Dudes'));

console.log(store.getState());


//Dispatching action 3
console.log('---------------------');
console.log('Dispatching action 3');
store.dispatch(addTodo('Hello my Friends'));

console.log(store.getState());


//Dispatching action 4
console.log('---------------------');
console.log('Dispatching action 4');
store.dispatch(toggleTodo(3));

console.log(store.getState());


//Dispatching action 5
console.log('---------------------');
console.log('Dispatching action 5');
store.dispatch(setVisibilityFilters('SHOW_COMPLETED'));

console.log(store.getState());


*/








