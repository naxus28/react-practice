//action creators
let nextId = 0;
const addTodo = (text) => {
	return {
		type: 'ADD_TODO',
		text
	}
}
const toggleTodo = (id) => {
	return {
		type: 'TOGGLE_TODO',
		id
	}
}
const setVisibilityFilters = (filter) => {
	return{
		type: 'SET_VISIBILITY_FILTER',
		filter
	}
}

//reducers
const todos = (state=[], action) => {
	switch(action.type){
		
		case 'ADD_TODO':
			return [
				...state,
				{
					id: nextId++,
					text: action.text,
					completed: false
				}
			]
		
		case 'TOGGLE_TODO':
		return state.map(todo=>{
			if(todo.id !== action.id){
				return todo;
			}

			return {
				...todo,
				completed: !todo.completed
			}
		});

		default:
			return state;
	}
}

const visibilityFilters = (state='SHOW_ALL', action) => {
	switch(action.type){
		case 'SET_VISIBILITY_FILTER':
			return action.filter;
		default:
			return state;
	}
}

//combine reducers
 const { combineReducers } = Redux; 

 const todoApp = combineReducers({
 	todos,
 	visibilityFilters
 });

//create store
const { createStore } = Redux;
const store = createStore(todoApp);

const { Component } = React;


const FilterLink = ({
	children,
	filter,
	currentFilter
}) => {
	if(filter === currentFilter){
		return <span>{children}</span>;
	}
	return(
		<a href="#"
			onClick={e => {
				e.preventDefault();
				store.dispatch(setVisibilityFilters(filter));
			}
		}>
			{children}
		</a>

	);
}

const getVisibleTodos = (
	todos, 
	filter
) => {
	switch(filter){
		case 'SHOW_ALL':
			return todos;

		case 'SHOW_COMPLETED':
			return todos.filter(
				t => t.completed
			);
		case 'SHOW_ACTIVE':
			return todos.filter(
				t => !t.completed
			);
	}
	
}

class TodoApp extends Component {
	render() {
		const {
			todos,
			visibilityFilters
		} = this.props;

		const visibleTodos = getVisibleTodos(
			todos,
			visibilityFilters
		);

		return(
			<div>
				<input type="text" ref={ node=>{
					this.input = node;
				}}/>
				<button onClick={() => {
					if(this.input.value=='')
					{
						alert('Enter a value');
						return;
					}
					else
					{
						store.dispatch(addTodo(this.input.value));
					}
					
					this.input.value='';//clears the input after an action is dispatched
				}}>
					Add Todo
				</button>
				<ul>
					{visibleTodos.map(todo=>
						<li key={todo.id} onClick={()=>{
							store.dispatch(toggleTodo(todo.id))
						}}
						style={{textDecoration: todo.completed? 'line-through': 'none', cursor: 'pointer'}}>
							{todo.text}
						</li>
					)}
				</ul>
				<p>
					Show: 
					{' '}
					<FilterLink
						filter='SHOW_ALL'
						currentFilter={visibilityFilters}
					>
							All
					</FilterLink>
					{' | '}
					<FilterLink
						filter='SHOW_COMPLETED'
						currentFilter={visibilityFilters}
					>
							Completed
					</FilterLink>
					{' | '}
					<FilterLink
						filter='SHOW_ACTIVE'
						currentFilter={visibilityFilters}
					>
							Not Completed
					</FilterLink>
				</p>
			</div>
		);
	}
}

const render = () => {
	ReactDOM.render(
		<TodoApp {...store.getState()}/>,
		document.getElementById('root')
	);
}

store.subscribe(render);

render();

/***************************CONSOLE LOGS***************************/
/*
//Initial state
console.log('Initial State');
console.log(store.getState());


//Dispatching action 1
console.log('---------------------');
console.log('Dispatching action 1');
store.dispatch(addTodo('Hello World'));

console.log(store.getState());


//Dispatching action 2
console.log('---------------------');
console.log('Dispatching action 2');
store.dispatch(addTodo('Hello Dudes'));

console.log(store.getState());


//Dispatching action 3
console.log('---------------------');
console.log('Dispatching action 3');
store.dispatch(addTodo('Hello my Friends'));

console.log(store.getState());


//Dispatching action 4
console.log('---------------------');
console.log('Dispatching action 4');
store.dispatch(toggleTodo(3));

console.log(store.getState());


//Dispatching action 5
console.log('---------------------');
console.log('Dispatching action 5');
store.dispatch(setVisibilityFilters('SHOW_COMPLETED'));

console.log(store.getState());


*/








