import fetch from 'isomorphic-fetch'

console.log(fetch);

export const SEARCH_PHOTOS 	= 'SEARCH_PHOTOS'
export const RECEIVE_PHOTOS = 'RECEIVE_PHOTOS'

export const searchPhotos = (keyword) => {
  return {
    type: SEARCH_PHOTOS,
    keyword
  }
}

const receivePhotos = (data) => {
	console.log('photos in receivePhotos: ', data.photos.photo);
  return {
    type: RECEIVE_PHOTOS,
    photos: data.photos.photo
  }
}

const getPhotos = () => {
	return dispatch => {
		return fetch(`https://api.flickr.com/services/rest/?method=flickr.photos.getRecent&api_key=1d3cddfecb2b92af4908394508288a7c&format=json&nojsoncallback=1&auth_token=72157662402574321-21ef811ea14bde14&api_sig=fd60f62656e533c44df1e1321590ee47`)
		            .then(response => response.json())
		            .then(data => dispatch(receivePhotos(data)))
	}
	
}

export const fetchPhotos = () => {
	return (dispatch, getState) => {
	      return dispatch(getPhotos())
	    }
}
