import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { searchPhotos, receivePhotos, fetchPhotos } from '../actions/index'
import PhotoList from '../components/PhotoList'
import SearchBar from '../components/SearchBar'

// console.log(fetchPhotos);

class App extends Component {
  constructor(props) {
    super(props)
    // this.handleLoadApi = this.handleLoadApi.bind(this)
    // this.handleLoadApiSearch = this.handleLoadApiSearch.bind(this)
  }
  componentDidMount() {
    const { dispatch, keyword } = this.props
    dispatch(fetchPhotos(keyword))
  }

  // componentWillReceiveProps(nextProps) { 
  //   if (nextProps.selectedReddit !== this.props.selectedReddit) {
  //     const { dispatch, selectedReddit } = nextProps
  //     dispatch(fetchPostsIfNeeded(selectedReddit))
  //   }
  // }

  // handleLoadApi(nextKeyword) {
  //   this.props.dispatch(fetchPhotos(nextKeyword))
  // }

  // handleLoadApiSearch(e) {
  //   e.preventDefault()
  //   const { dispatch, keyword } = this.props
  //   dispatch(fetchPhotos(keyword))
  // }

  render() {
    const { photos, keyword } = this.props
    console.log('this.props:', this.props);

    return (
    		<div>
    			<SearchBar />
    			<PhotoList photos={photos} />;
    		</div>
    	);
  }
}

// App.propTypes = {
//   selectedReddit: PropTypes.string.isRequired,
//   posts: PropTypes.array.isRequired,
//   isFetching: PropTypes.bool.isRequired,
//   lastUpdated: PropTypes.number,
//   dispatch: PropTypes.func.isRequired
// }


function mapStateToProps(state) {
console.log('state in mapStateToProps', state);
  const { keywordInput, receivedPhotos } = state
  const photos = state.receivedPhotos
  const keyword = state.keywordInput
  return {
  	photos,
    keyword
  }
}

export default connect(mapStateToProps)(App)