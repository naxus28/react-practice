import { combineReducers } from 'redux'

//import action descriptions
import {
	SEARCH_PHOTOS,
	RECEIVE_PHOTOS
} from '../actions/index'


const keywordInput = (state = 'beach', action) => {
	switch (action.type) {
		case SEARCH_PHOTOS:
		  return action.keyword
		default:
		  return state
	}
}

const receivedPhotos = (state = {}, action) => {
	
	switch (action.type) {
	    case RECEIVE_PHOTOS:
	    console.log('state in receivedPhotos: ', state);
	    console.log('reducer: ', action.photos)
	      return action.photos

	    default:
	      return state
  }
}

const rootReducer = combineReducers({
	keywordInput,
	receivedPhotos
});

export default rootReducer