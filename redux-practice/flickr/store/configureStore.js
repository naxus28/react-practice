import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import createLogger from 'redux-logger'
import rootReducer from '../reducers/index'

const createStoreWithMiddleware = applyMiddleware(
  thunkMiddleware, // lets us dispatch() functions
  createLogger()// neat middleware that logs actions to the console--not directly related to the app's async functionality
)(createStore)//has the 3 redux's fundamental methods


// 'configureStore' will be called in '/index.js' without any arguments: const store = configureStore()
// so we don't really need 'initialState' as a parameter--
// it doesn't do anything for us but the code came like this from Redux docs
export default function configureStore() {
 return createStoreWithMiddleware(rootReducer);
  // if (module.hot) {
  //   // Enable Webpack hot module replacement for reducers
  //   module.hot.accept('../reducers', () => {
  //     const nextRootReducer = require('../reducers')
  //     store.replaceReducer(nextRootReducer)
  //   })
  // }

  // return store
}