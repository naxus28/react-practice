import React, { PropTypes, Component } from 'react'

export default class SearchBar extends Component {
  render() {
    return (
     	<form onSubmit={this.handleSubmit} className="form" ref="form">
     	  <input  ref="keyword" type="text" placeholder="Keyword"/>
     	  <button>Search Images on Flickr</button>
     	</form>
    )
  }
}
