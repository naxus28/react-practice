import React, { Component, PropTypes } from 'react'


export default class Photo extends Component {
  render() {
    const { key, href, src, alt, className, imgclassName } = this.props
    return (
      <article>
  		<div className={className} key={key}>
      			<a href={href} target='_blank'>
	      				<img 
	      					src={src} 
	      					alt={alt} 
	      					className={imgclassName}/>
      			</a>
  		</div>
      </article>
    )
  }
}