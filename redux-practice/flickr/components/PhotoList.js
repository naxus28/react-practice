import React, { PropTypes, Component } from 'react'
import Photo from './Photo'
import SearchBar from './SearchBar'

export default class PhotoList extends Component {
  render() {
  	const { photos } = this.props
  	console.log('photos in PhotoList', photos);
  	console.log('photos length', photos.length);
  	if(photos.length>0)
  	{
  		return(
  			<div>	
  		      	<h2 className='header'>Flickr API App</h2>
  		      	<SearchBar />
  				{photos.map((photo, index) =>
  					<Photo 
  						key={index} 
  						className="imageWrapper"
  						target={"_blank"}
  						href={"https://farm"+photo.farm+".staticflickr.com/"+photo.server+"/"+photo.id+"_"+photo.secret+".jpg"}
  						imgclassName={"images"} 
  						src={"https://farm"+photo.farm+".staticflickr.com/"+photo.server+"/"+photo.id+"_"+photo.secret+".jpg"} 
  						alt={photo.title} />
  				)}
  			</div>
  		);
  	}
	else{
		return <p className="loading">Loading...</p>;
	}
  }
}

// PhotoList.propTypes = {
//   photos: PropTypes.array.isRequired
// }