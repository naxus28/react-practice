//action types
const INCREMENT = 'INCREMENT';
const DECREMENT = 'DECREMENT';

//actions
const increment = () => {
	return{
		type: INCREMENT
	}
}
const decrement = () => {
	return{
		type: DECREMENT
	}
}

//reducer	
const appCounter = (state=0, action) =>{
	switch(action.type){

		case INCREMENT:
			return state+1;

		case DECREMENT:
			return state-1;

		default:
			return state;
	}
}

//get react's connect method 
const { connect } = ReactRedux;

//create store--it will be passed down directly from the 'Provider'
const { createStore } = Redux;

//Counter presentational component
const Counter = ({state}) => (
	<div>
		<span style={{width:'80', padding:'10', paddingLeft: '0', marginRight: '5', display: 'inline-block'}}>
			Counter: {state}{' '}
		</span>
		<span>
			This number is: {state%2==0? 'even':'odd'}
		</span>
	</div>
);

//CounterContainer container component
//gets 'state' and 'dispatch' from 'connect' method just below
let CounterContainer = ({state, dispatch}) =>(
	<div>
		<button 
			style={{width: '25', height:'25', background: '#5cb85c', border: '1px solid #4cae4c', borderRadius:'2', cursor: 'pointer'}} 
			onClick={()=>
			dispatch(increment())
		}>+</button>
		<button 
			style={{width: '25', height:'25', background: '#d9534f', border: '1px solid #d43f3a', borderRadius:'2', cursor: 'pointer'}} 
			onClick={()=>
			dispatch(decrement())
		}>-</button>
		<Counter state={state}/>
	</div>
);

//merges the returned state object and dispatch object and passes them to the element wrapped (connect's second argument)
//here we we basically wrap CounterWrapper in itself passing 'state' and 'dispatch' provided by 'connect'
CounterContainer = connect(
	state =>{
		return{
			state: state
		};
	},
	dispatch =>{
		return{
			dispatch: dispatch
		};
	}
)(CounterContainer);

//Provider provides the context that will be passed to its children and grandchildren
const { Provider } = ReactRedux;

//render method will be called only once because react's 'connect' 
//has all store methods (getState, dispatch, and subscribe) out of the box
//thus it will re-render for every action dispatched as long as the dispatched action is made via 'connect'
ReactDOM.render(
	<Provider store = {createStore(appCounter)}>
		<CounterContainer />
	</Provider>,
	document.getElementById('root')
);