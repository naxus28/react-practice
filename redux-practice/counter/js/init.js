//action constants
const INCREMENT = 'INCREMENT';
const DECREMENT = 'DECREMENT';

//actions
const increment = () => {
	return{
		type: INCREMENT
	}
}
const decrement = () => {
	return{
		type: DECREMENT
	}
}

//reducer	
const appCounter = (state=0, action) =>{
	switch(action.type){

		case INCREMENT:
			return state+1;

		case DECREMENT:
			return state-1;

		default:
			return state;
	}
}

//create store
const { createStore } = Redux;
const store = createStore(appCounter);

//component
const Counter = ({ state }) =>(
	<div>
		Counter: {state} {' '}
		<button onClick={()=>
			store.dispatch(increment())
		}>+</button>
		<button onClick={()=>
			store.dispatch(decrement())
		}>-</button>
	</div>

);

const render = () =>{
	ReactDOM.render(
		<Counter state={store.getState()}/>,
		document.getElementById('root')
	);
}
render();
store.subscribe(render);