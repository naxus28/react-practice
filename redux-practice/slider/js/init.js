//action types
const SLIDER   = 'SLIDER';
const SLIDER_ONE   = 'SLIDER_TWO';
const SLIDER_TWO = 'SLIDER_THREE';

//action
const slide = (sliderNumber, value) => {
	return {
		type: sliderNumber,
		value,
		id
	}
}

//reducer
let id;
const sliderApp = (state={}, action) => {

	switch(action.type){
		
		case SLIDER:
			return {id: 0,
				...state,
				action.value
			}

		case SLIDER_ONE:
		return {1,
				...state,
				action.value
			}
		case SLIDER_TWO:
			return {2,
				...state,
				action.value
			}

		default:
			return state;
	}
}

//store
const { createStore } = Redux;

const store = createStore(sliderApp);

//react component function--returns obj
const { Component } = React;

//provider method--exposes the store to children and grandchildren
const { Provider } = ReactRedux;

//provides the 3 redux methods to the component passed as 2nd argument to the function
const { connect } = ReactRedux;

console.log(store.getState());

store.dispatch(slide(SLIDER, 10))

console.log(store.getState());


store.dispatch(slide(SLIDER_ONE, 10))
console.log(store.getState());

// //presentational component
// const Color = ({color}) =>(
// 	<div style={{width: '140', height: '100', 
// 				marginTop: '10px', border: '1px solid black', 
// 				background: color}}>
// 	</div>
// );

// //container component
// class ColorBox extends Component {
	
// 	componentDidMount() {
// 		const { store } = this.context;
// 		this.unsubscribe = store.subscribe(() =>
// 			this.forceUpdate()
// 		);
// 	}

// 	componentWillUnmount() {
// 		this.unsubscribe();
// 	} 

// 	render(){
// 		const { store } = this.context;
// 		const state = store.getState();
// 		return(
// 			<div>
// 				<Color color={'rgb('+state[0]+','+state[1]+','+state[2]+')'} />	
// 				<p>This is your RGB color code: rgb({state[0]}, {state[1]}, {state[2]})</p>
// 			</div>
// 		);
// 	}
// }

// ColorBox.contextTypes = {
// 	store: React.PropTypes.object
// }
// //presentational component
// const Slider = ({
// 	state, 
// 	onSlide
// }) => {
// 	let input=0;
// 	return(
// 		<div>
// 			<input 
// 				type="range" 
// 				min='0'
// 				max='255'
// 				value={state}
// 				ref={node => {
// 					input=node;
// 				}} 
// 				onChange={()=> onSlide(input.value)}/>

// 			<span>{state}</span>
// 		</div>
// 	);
// }

// let index;
// const getSliderindex = (slider) => {
// 	switch(slider){
// 		case SLIDER:
// 			return index=0;

// 		case SLIDER_ONE:
// 			return index=1;

// 		case SLIDER_TWO:
// 			return index=2;

// 		default:
// 			return state;
// 	}
// }
// // //container component
// const mapStateToSlider = (state) => {
// 	return {
// 		state: state[state.id]
// 	}
// }
// const mapDispatchToSlider = (dispatch) => {
// 	return {
// 		onSlide: (input) => {
// 			dispatch(slide(SLIDER,input, id))
// 		} 
// 	}
// }
// // const Sliders = connect(
// // 	mapStateToSlider,
// // 	mapDispatchToSlider
// // )(Slider);

// class Sliders extends Component {
// 	componentDidMount() {
// 		const { store } = this.context;
// 		this.unsubscribe = store.subscribe(() =>
// 			this.forceUpdate()
// 		);
// 	}
// 	componentWillUnmount() {
// 		this.unsubscribe();
// 	} 

// 	render(){
// 		const { store } = this.context;
// 		const state = store.getState();

// 		return(
// 			<div>
// 				<p>Slide to change the box color:</p>
// 				<Slider 
// 					state={state[0]}
// 					onSlide={input => {
// 						store.dispatch(slide(SLIDER,input, 0))
// 					}} />
				
// 				<Slider 
// 					state={state[1]}
// 					onSlide={input => {
// 						store.dispatch(slide(SLIDER_ONE, input, 1))
// 					}} />
				
// 				<Slider 
// 					state={state[2]}
// 					onSlide={input => {
// 						store.dispatch(slide(SLIDER_TWO, input, 2))
// 					}} />
// 			</div>

// 		);
// 	}
// }

// Sliders.contextTypes = {
// 	store: React.PropTypes.object
// }

// //container component
// const SliderApp = () => {
// 	return(
// 		<div>	
// 			<Sliders />
// 			<ColorBox />		
// 		</div>
// 	);
// }

// //render controller app
// ReactDOM.render(
// 	<Provider store={createStore(sliderApp)}>
// 		<SliderApp />
// 	</Provider>,
// 	document.getElementById('root')
// );
