//loops over the images arrays and passes to the image component
var ImagesList = React.createClass({
  render:function(){
    // var count = this.props.count;
    var images = this.props.images;

    //test if the object is not empty--it is empty on the first render, which throws an error on map prototype(there is nothing to loop over)
    if(Object.keys(images).length)
    {
      //loops over arrays of images and extract the values necessary to build the image
      var imageCollection=this.props.images.map(function(image, index){
          return(
            <div key={index} className="imageWrapper">
              <a target="_blank" href={"https://farm"+image.farm+".staticflickr.com/"+image.server+"/"+image.id+"_"+image.secret+".jpg"}><img className="images" key={index} src={"https://farm"+image.farm+".staticflickr.com/"+image.server+"/"+image.id+"_"+image.secret+".jpg"} alt={image.title} /></a>
            </div>
            
          ); 
      });
    }
    return(
      <div>
        <Image images={imageCollection} />
      </div>
      
    );
  }
});