 /********************************************************************************
      __                                         _____                               
    /    )         /           ,         /       /    '                              
---/---------__---/__---)__--------__---/-------/__-------__---)__---)__----__----__-
  /  --,   /   ) /   ) /   ) /   /___) /       /        /___) /   ) /   ) /   )     /
_(____/___(___(_(___/_/_____/___(___ _/_______/________(___ _/_____/_____(___(____(__
                                                                                  /  
                                                                              (_ / 
* DEVELOPER: Gabriel Ferraz                                           
* COMPANY: Full Sail University
* DATE: 11/20/15
* 
* FUNCTIONALITY                                                           
* Retrive images from the Flickr search photos API via JSON                        
*                                                                         
* REFERENCES                                                             
* API CALL OPTIONS: https://www.flickr.com/services/api/explore/flickr.photos.search
* API URL ARGUMENTS: https://www.flickr.com/services/api/flickr.photos.search.htm
* API URL FORMATS: https://www.flickr.com/services/api/misc.urls.html             
*************************************************************************************/

//passes the images array to the image list component (get arrays of images from the object; map prototype will need the arrays to loop; passing the object makes it impossible to loop) 

var App = React.createClass({
    propTypes: {
      keyword: React.PropTypes.string
    },
    getDefaultProps:function() {
      return {keyword:'beach',
              header:'beach'};
    },
    handleSearch: function(keyword){
      console.log("keyword on controller app: ", keyword);
      this.setProps({header:keyword});//deprecated
      this.loadApi(keyword);
    },
    loadApi: function(keyword){
        //api url
        if(!keyword)
        {
          keyword==this.props.keyword;
        }
        var url = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=439d3c788d4c522138a62a1a9faf2f10&tags="+keyword+"&text="+keyword+"&format=json&nojsoncallback=1";
        //var that bings 'this' to the AJAX scope--perhaps could use "bind(this)" as well
        var self=this;
        $.getJSON(url, function(data){
            //set the new state of data to the arrays of photos--not the raw object returned (not 'data')
            self.setState({images: data.photos.photo});
        });
      },
    componentDidMount:function() {
      //load images just after component mounts initial state
      this.loadApi();
    },
    getInitialState:function(){
      return {images: {}}
    },
    render:function(){
      return ( 
        <div>
        <h2 className="header">Displaying Images for Keyword "{this.props.header}"</h2>
          <Image header={this.props.header}/>
          <SearchBar onSearch={this.handleSearch} />
          <ImagesList images={this.state.images} />
        </div>
        
      );
    }
  });