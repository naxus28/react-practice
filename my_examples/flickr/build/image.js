//renders the images collection
var Image = React.createClass({
  displayName: "Image",

  render: function () {
    return React.createElement(
      "article",
      null,
      React.createElement(
        "section",
        null,
        this.props.images
      )
    );
  }
});