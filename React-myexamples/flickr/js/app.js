 /********************************************************************************
      __                                         _____                               
    /    )         /           ,         /       /    '                              
---/---------__---/__---)__--------__---/-------/__-------__---)__---)__----__----__-
  /  --,   /   ) /   ) /   ) /   /___) /       /        /___) /   ) /   ) /   )     /
_(____/___(___(_(___/_/_____/___(___ _/_______/________(___ _/_____/_____(___(____(__
                                                                                  /  
                                                                              (_ / 
* DEVELOPER: Gabriel Ferraz                                           
* COMPANY: Full Sail University
* DATE: 11/20/15
* 
* FUNCTIONALITY                                                           
* Retrive images from the Flickr search photos API via JSON                        
*                                                                         
* REFERENCES                                                             
* API CALL OPTIONS: https://www.flickr.com/services/api/explore/flickr.photos.search
* API URL ARGUMENTS: https://www.flickr.com/services/api/flickr.photos.search.htm
* API URL FORMATS: https://www.flickr.com/services/api/misc.urls.html             
*************************************************************************************/

///passes the images array to the image list component (get arrays of images from the object; map prototype will need the arrays to loop; passing the object makes it impossible to loop)
    var App = React.createClass({
      propTypes: {
        keyword: React.PropTypes.string
      },
      getDefaultProps:function() {
        return {
          keyword:'beach'
        };
      },
      handleSearch: function(keyword){
        console.log("keyword on controller app: ", keyword);
        this.setState({
          keyword: keyword
        }, this.loadApi());
      },
      loadApi: function(){
        //api url
        var url = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=439d3c788d4c522138a62a1a9faf2f10&tags="+this.state.keyword+"&text="+this.state.keyword+"&format=json&nojsoncallback=1";
        //var that binds 'this' to the AJAX scope--perhaps could use "bind(this)" as well
        var self=this;
        $.getJSON(url, function(data){
            //set the new state of data to the arrays of photos--not the raw object returned (not 'data')
            self.setState({images: data.photos.photo});
        });
      },
      componentDidMount:function() {
        //load images just after component mounts initial state
        this.loadApi();
      },
      getInitialState:function(){
        return {
          images: [],
          keyword: this.props.keyword
        }
      },
      render:function(){
        return (
          <div>
          <h2 className="header">Displaying Images for Keyword "{this.state.keyword}"</h2>
            <Image header={this.props.header}/>
            <SearchBar onSearch={this.handleSearch} />
            <ImagesList images={this.state.images} />
          </div>

        );
      }
    });

    //search form
    var SearchBar = React.createClass({
      componentDidMount: function(){
        $(this.refs.form).draggable();
      },
      handleSubmit: function(e){
        e.preventDefault();

        //use 'refs' to get value from input on form submission
        var keyword = this.refs.keyword.value.trim();

        //validate form: user needs to pass a string
        if(!keyword)
        {
          alert('You must enter a keyword');
          return;
        }

        //clear input field after submit
        this.refs.keyword.value="";

        //callback passes keyword as props to parent component App
        this.props.onSearch(keyword);
      },
      render:function(){
        return(
          <form onSubmit={this.handleSubmit} className="form" ref="form">
            <input  ref="keyword" type="text" placeholder="Keyword"/>
            <button>Search Images on Flickr</button>
          </form>
        );
      }
    });

    //loops over the images arrays and passes to the image component
    var ImagesList = React.createClass({
      render:function(){
        var images = this.props.images;

        console.log("THE IMAGES OBJECT: " + images);
        //loops over arrays of images and extract the values necessary to build the image
        var imageCollection=images.map(function(image, index){
            return(
              <div key={index} className="imageWrapper">
                <a target="_blank" href={"https://farm"+image.farm+".staticflickr.com/"+image.server+"/"+image.id+"_"+image.secret+".jpg"}><img className="images" key={index} src={"https://farm"+image.farm+".staticflickr.com/"+image.server+"/"+image.id+"_"+image.secret+".jpg"} alt={image.title} /></a>
              </div>

            );
        });

        return(
          <div>
            <Image images={imageCollection} />
          </div>

        );
      }
    });

    //renders the images collection
    var Image = React.createClass({
      render:function(){
        return(
          <article>
            <section>{this.props.images}</section>
          </article>

        );
      }
    });

    ReactDOM.render(<App />, document.getElementById('example'));
