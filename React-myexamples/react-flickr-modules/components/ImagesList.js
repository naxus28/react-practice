import React from 'react'
import Images from './Images'

//loops over the images arrays and passes to the image component
var ImagesList = React.createClass({
  render(){
    var images = this.props.images;
    // console.log('images: ', images)

    // console.log("THE IMAGES OBJECT: "+ images);
    //loops over arrays of images and extract the values necessary to build the image
    var imageCollection=images.map(function(image, index){
        return(
          <div key={index} className="imageWrapper">
            <a target="_blank" 
                href={"https://farm"+image.farm+".staticflickr.com/"+image.server+"/"+image.id+"_"+image.secret+".jpg"}>
              <img className="images" 
                    key={index} 
                    src={"https://farm"+image.farm+".staticflickr.com/"+image.server+"/"+image.id+"_"+image.secret+".jpg"} 
                    title={image.title} 
                    alt={image.title} />
            </a>
          </div>

        );
    });
    return(
      <div>
        <Images images={imageCollection} />
      </div>

    );
  }
});

export default ImagesList;