import React from 'react'

//search form
var SearchBar = React.createClass({
  handleSubmit: function(e){
    e.preventDefault();

    //use 'refs' to get value from input on form submission
    var keyword = this.refs.keyword.value.trim();

    //validate form: user needs to pass a string
    if(!keyword)
    {
      alert('You must enter a keyword');
      return;
    }

    //clear input field after submit
    this.refs.keyword.value="";

    //callback passes keyword as props to parent component App
    this.props.onSearch(keyword);
  },

  render(){
    return(
      <form onSubmit={this.handleSubmit} className="form" ref="form">
        <input  ref="keyword" type="text" placeholder="Keyword"/>
        <button>Search Images on Flickr</button>
      </form>
    );
  }
});

export default SearchBar;