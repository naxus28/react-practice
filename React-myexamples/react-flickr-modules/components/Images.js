import React from 'react'

//renders the images collection
var Images = React.createClass({
  render(){
    return(
      <article>
        <section>{this.props.images}</section>
      </article>

    );
  }
});

export default Images;