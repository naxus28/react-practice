import React from 'react'
import ImagesList from '../components/ImagesList'
import SearchBar from '../components/SearchBar'

var App = React.createClass({

	//sets initial state of the app
	//uses getDefaultProps to get the initial state for the keyword
	getInitialState:function(){
	return {
			images: [],
			keyword: '',
			isFetching: true,
			fetchingMessage:'',
			apiError: false,
			errorMessage: ''
		}
	},

	//called just after component mounts initial state
	//invokes this.fetching(), which will invoke loadApi()
	componentDidMount:function() {
		this.fetching();
	},

	/*
		Invoked immediately before rendering when new props or state are being received. This method is not called for the initial render.
		Use this as an opportunity to perform preparation before an update occurs.
	*/
	componentWillUpdate: function(nextProps, nextState) {
		console.log('componentWillUpdate');
		console.log('this.state: ', this.state);
		console.log('nextState: ', nextState);
		console.log('-----------------------');
	},

	/*
		Invoked immediately after the component's updates are flushed to the DOM. This method is not called for the initial render.
		Use this as an opportunity to operate on the DOM when the component has been updated.
	*/
	componentDidUpdate: function(prevProps, prevState) {
		console.log('componentDidUpdate');
		console.log('prevState: ', prevState);
		console.log('this.state: ', this.state);
		console.log('-----------------------');
		if(prevState.keyword !== this.state.keyword)
		{
			this.fetching();
		}
	},

	//sets 'isFetching' to true and signals UI to show spinner
	//invokes 'loadApi()'
	fetching:function() {
		this.setState({
			isFetching: true,
			fetchingMessage:'Loading Photos...',
			apiError: false,
		}, this.loadApi());
	},

	//load the api when component mounts and when users performs a search
	loadApi: function(){
		//decide which api should be loaded
		//if there is no keyword, load getRecent API; if there is, load searh photos api
		var url = this.state.keyword? "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=bd1dcb80870ea3427e8b693c2ef79eb1&tags="+this.state.keyword+"&text="+this.state.keyword+"&format=json&nojsoncallback=1"
									 :"https://api.flickr.com/services/rest/?method=flickr.photos.getRecent&api_key=d037cd75fa02275e99f22c2b1bba5bd2&format=json&nojsoncallback=1";
	
		var self=this;//bind 'this' to the the AJAX scope--perhaps could use "bind(this)" as well

		$.ajax({
		   url: url,
		   dataType: 'json',
		   success: function( data ) {
		    self.setState({
		    	images: data.photos.photo, //set the new state of data to the arrays of photos--don't use the raw object returned ('data')
		    	isFetching: false,
		    	fetchingMessage:'',
		    	apiError: false,
		    	errorMessage:''
		    });
		   },
		   error: function( data ) {
		     self.setState({
		       isFetching: false,
		       apiError: true,
		       errorMessage: data.message
		     });
		   }
		 });
	},

	//called when user submits a search
	//sets keyword's new state
	handleSearch: function(keyword){
		this.setState({
			keyword:keyword
		});
	},

	//render the markup
	render(){
		return (
		  <div>
				{/*if there is an error*/}
		    {this.state.apiError &&
		    !this.state.isFetching &&
		    	<p className='apiError'>{this.state.errorMessage}</p>
		    }

			{/*if api is fetching*/}
		    {!this.state.apiError &&
		    this.state.isFetching &&
		    	<div>
		    		<p className='apiFetching'>{this.state.fetchingMessage}</p>
		    		<p className='spinner'><i className="fa fa-4x fa-spinner fa-spin" /></p>
		    	</div>
		    }

			{/*if api call is successful but returns no images*/}
		    {!this.state.isFetching &&
		    !this.state.apiError &&
		    this.state.images===0 &&
		    	<div>
		    		<h1 className="header">Flickr Photo Wall</h1>
		    		<h2 className="header">No images found for "{this.state.keyword}"</h2>
					<SearchBar onSearch={this.handleSearch} />
		    	</div>
		    	
		    }

			{/*if api call is successful and returns images*/}
		    {!this.state.isFetching &&
		    !this.state.apiError &&
		    	<div>
		    		<h1 className="header">Flickr Photo Wall</h1>
		    		{this.state.keyword &&
		    			<h2 className="header">Displaying Images for Keyword "{this.state.keyword}"</h2>
		    		}
		    		{!this.state.keyword &&
		    			<h2 className="header">Displaying Images from getRecent API</h2>
		    		}
		    		
					<SearchBar onSearch={this.handleSearch} />
				  	<ImagesList images={this.state.images} />
		    	</div>
		    	
		    }
		  </div>
		);
	}
});

export default App;