//reducer function
const counter = (state=0, action) => {
	switch (action.type)
	{
		case 'INCREMENT':
			return state+1;
		case 'DECREMENT':
			return state-1;
		default:
			return state;
	}
}

//store
const { createStore } = Redux;

//passes the reducer into the store, which will allow to tell the reducer how it will update the state via actions
const store = createStore(counter);

//the react component can be a function since React 14
const Counter = ({ value, onIncrement, onDecrement }) => (
	<div>
		<h1>{value}</h1>
		<button onClick={onDecrement}>-</button>
		<button onClick={onIncrement}>+</button>
	</div>
);	

//renders the React root component
const render = () => {
	ReactDOM.render(
		<Counter 
			value={store.getState()} 
			onDecrement={()=>
				store.dispatch({
					type: 'DECREMENT'
				})}
			onIncrement={()=>
				store.dispatch({
					type: 'INCREMENT'
				})}  />, 
		document.getElementById('root')
	);
};

//puts the initial state on the screen before we start updating it
render();

//registers a call back that will be called anytime an action has been dispatched
store.subscribe(render);