//main reducer--controller
const todoApp = (state = {}, action) => {
	console.log("TODO APP"); 
	console.log("state: ", state);
	console.log("state.todos: ", state.todos);
	console.log("state.visibilityFilter: ", state.visibilityFilter);
	console.log("action: ", action);

	// this returns the state of the app as a whole
	// it delegates specific actions to the children reducers
	// each child reducer manages separate parts of the app
	// the main reducer--this one--combines the children reducer and returns one object with the state of the whole app
	// no need to modify the children reducers when we need to add an action to the app

	return {
		todos: todos(
			// This is the state of this key in this object. Its initial state is an empty array
			// it represents the state of the todos in the whole app
			// state.todos will be populated every time an action is dispatched with an action.type of 'ADD_TODO' or 'TOGGLE_TODO'
			// first we need to dispatch an action whose type is 'ADD_TODO', otherwise we can't toggle it or change its visibility
			// the key can have any other name as long as we pass that name as the part of the state we want (i.e. 'x: todos(state.x, action)') 
			state.todos, 
			action
		),
		visibilityFilter: visibilityFilter(
			state.visibilityFilter,
			action)
	}
}


//reducer that manages all todos
const todos = (state=[], action) => {
	switch(action.type){
		case 'ADD_TODO':
			return [
				...state,
				todo(undefined, action)
			];

		case 'TOGGLE_TODO':
			console.log('Chechinkg object passed on toggle todo switch');
			state.map(t=>console.log(t));
			return state.map(t=>todo(t, action)); /*t= [{id: 0, text: "Learn Redux", completed: false},
														{id: 1, text: "Go Shopping", completed: false}]*/
		
		default:
			return state;
	}
}

//reducer that manages single 'todo' component
const todo = (state, action) => {
	switch(action.type){
		case 'ADD_TODO':
			return {
					id: action.id,
					text: action.text,
					completed: false
			}
		
		case 'TOGGLE_TODO':
		//this comes from a map function, which has multiple objects
		//if the id property of the obj tested in the loop is different from the one passed in the action,
		//return the state as is
			if(state.id !== action.id){
				return state;
			}
			//if the id property of the object and the id property of the action are different
			//return the state but flip the 'completed' field
			return {
				...state,
				completed: !state.completed
			}

		default:
			state;	
	}
}

//visibility reducer
const visibilityFilter = (state='SHOW_ALL', action) => {
	switch(action.type){
		case 'SET_VISIBILITY_FILTER':
			return action.filter;
		default:
			return state;
	}
}


const { createStore } = Redux;

const store = createStore(todoApp);

//*************
console.log('Current State');
console.log(store.getState());
console.log('---------------');

//*************
console.log('Dispatching ADD_TODO');
store.dispatch({
	id: 0,
	type: 'ADD_TODO',
	text: 'Learn Redux'
});
console.log(store.getState());
console.log('---------------');

//*************
console.log('Dispatching ADD_TODO');
store.dispatch({
	id: 1,
	type: 'ADD_TODO',
	text: 'Go Shopping'
});
console.log(store.getState());
console.log('---------------');

//*************
console.log('Dispatching TOGGLE_TODO');
store.dispatch({
	id: 0,
	type: 'TOGGLE_TODO',
});
console.log(store.getState());
console.log('---------------');

//*************
console.log('Dispatching SET_VISIBILITY_FILTER');
store.dispatch({
	type: 'SET_VISIBILITY_FILTER',
	filter: 'SHOW_COMPLETED'
});
console.log('getting state');
console.log(store.getState());
console.log('---------------');





//*************TESTS*************

//test function
const testTodo = () => {
	//initial state
	const stateBefore = [];

	//action to be passed
	const action = {
		id: 0,
		type: 'ADD_TODO',
		text: 'Learn Redux'
	}

	//what we expect as new state (not a mutation of initial state, but a new object altogether)
	const stateAfter = [
		{
			id: 0,
			text: 'Learn Redux',
			completed: false
		}
	];

	deepFreeze(stateBefore);
	deepFreeze(action);
	
	//by passing "stateBefore" and "action" into todos function we expect the result to equal "stateAfter"
	expect(
		todos(stateBefore, action)
	).toEqual(stateAfter);
}

//test function
const testToggleTodo = () => {
	//initial state
	const stateBefore = [
		{
			id: 0,
			text: 'Learn Redux',
			completed: false
		},
		{
			id: 1,
			text: 'Go Shopping',
			completed: false
		}
	];

	//action to be passed
	const action = {
		id: 1,
		type: 'TOGGLE_TODO',
	}

	//array we expect as new state (not a mutation of initial state, but a new object altogether)
	const stateAfter = [
		{
			id: 0,
			text: 'Learn Redux',
			completed: false
		},
		{
			id: 1,
			text: 'Go Shopping',
			completed: true
		}
	];

	//freeze the state and the action to make sure we don't mutate them accidentally
	deepFreeze(stateBefore);
	deepFreeze(action);

	//by passing "stateBefore" and "action" into the 'todos' function we expect the result to equal "stateAfter"
	expect(
		todos(stateBefore, action)
	).toEqual(stateAfter);
}

testTodo();
testToggleTodo();
console.log('Tests passed');