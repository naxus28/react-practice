// reducer takes the state and action
const todos = (state=[], action) => {
	switch(action.type){
		// returns the new state of objects passed to the reducer
		// new state is the old state (empty array) + the object constructed in the return statement
		// new state will then be a single array with one object in this case
		case 'ADD_TODO':
			return [
				...state,
				{
					id: action.id,
					text: action.text,
					completed: false
				}
			];
			
		default:
			state;
	}
}

//test function
const testTodo = () => {
	//initial state
	const stateBefore = [];

	//action to be passed
	const action = {
		id: 0,
		type: 'ADD_TODO',
		text: 'Learn Redux'
	}

	//what we expect as new state (not a mutation of initial state, but a new object altogether)
	const stateAfter = [
		{
			id: 0,
			text: 'Learn Redux',
			completed: false
		}
	];

	//by passing "stateBefore" and "action" into todos function we expect the result to equal "stateAfter"
	expect(
		todos(stateBefore, action)
	).toEqual(stateAfter);

	deepFreeze(stateBefore);
	deepFreeze(action);
}

testTodo();
console.log('Test passed');