//reducer component
const todo = (state, action) => {
	switch(action.type){
		case 'ADD_TODO':
			return {
					id: action.id,
					text: action.text,
					completed: false
			}
		
		case 'TOGGLE_TODO':
			//if the state.id is different than the action.id
			//just return the object
			//use it here because we are looping over all objects
			if(state.id !== action.id){
				return state;
			}


			//if the object state.id is the same as the action.id
			//return the object and flip the 'completed' key
			return {
				...state,
				completed: !state.completed
			}
			
		default:
			return state;	

	}
}

// reducer takes the state and action
const todos = (state=[], action) => {
	switch(action.type){
		// returns the new state
		//new state is the old state, represented by ...state (empty array in this case),  + the object constructed in the return statement
		//new state will then be a single object
		case 'ADD_TODO':
			return [
				...state,
				todo(undefined, action)
			];

		case 'TOGGLE_TODO':
			
			// state.map(t => console.log(t));
			// 't' represents each one of the array elements 
			// map loops over the two objects in the array and calls the todo reducer twice as well
			// passes the objects (state), and the action
			// map function returns a new array--we can't mutate the state
			return state.map(t=>todo(t, action));

			
		default:
			return state;
	}
}

//test function
const testTodo = () => {
	//initial state
	const stateBefore = [];

	//action to be passed
	const action = {
		id: 0,
		type: 'ADD_TODO',
		text: 'Learn Redux'
	}

	//what we expect as new state (not a mutation of initial state, but a new object altogether)
	const stateAfter = [
		{
			id: 0,
			text: 'Learn Redux',
			completed: false
		}
	];

	deepFreeze(stateBefore);
	deepFreeze(action);
	
	//by passing "stateBefore" and "action" into todos function we expect the result to equal "stateAfter"
	expect(
		todos(stateBefore, action)
	).toEqual(stateAfter);
}

//test function
const testToggleTodo = () => {
	//initial state
	const stateBefore = [
		{
			id: 0,
			text: 'Learn Redux',
			completed: false
		},
		{
			id: 1,
			text: 'Go Shopping',
			completed: false
		}
	];

	//action to be passed
	const action = {
		id: 1,
		type: 'TOGGLE_TODO',
	}

	//array we expect as new state (not a mutation of initial state, but a new object altogether)
	const stateAfter = [
		{
			id: 0,
			text: 'Learn Redux',
			completed: false
		},
		{
			id: 1,
			text: 'Go Shopping',
			completed: true
		}
	];

	//freeze the state and the action to make sure we don't mutate them accidentally
	deepFreeze(stateBefore);
	deepFreeze(action);

	//by passing "stateBefore" and "action" into the 'todos' function we expect the result to equal "stateAfter"
	expect(
		todos(stateBefore, action)
	).toEqual(stateAfter);
}

testTodo();
testToggleTodo();
console.log('Tests passed');