
// reducer that manages all todos
const todos = (state=[], action) => {
	switch(action.type){
		case 'ADD_TODO':
			return [
				...state,
				todo(undefined, action)
			];

		case 'TOGGLE_TODO':
			return state.map(t=>todo(t, action));
		
		case 'DELETE_TODO':
			console.log('state: ', state);
			console.log('action.id:', action.id);
			// console.log('state.id: ', state[action.id].id);
			
			let todoArrayposition=0;

			state.map((todo, index)=>{
				console.log("index:", index);
				
				if(action.id==todo.id)
				{
					todoArrayposition=index;
					console.log("todoArrayposition:", todoArrayposition);
				}
			});

			if(todoArrayposition==state[0].id || state.length==1) 
			{
				console.log('first element in the array');
				return [
					...state.slice(1, state.length) // we just care about what comes after it, even if it is empty (this happens when state.length==1)
				];
			}
			else
			{
				console.log('Slicing from beginning and end');
				return [
					...state.slice(0, todoArrayposition), // i.e 0 to 2 -- doesn't include 2
					...state.slice(todoArrayposition+1, state.length) //i.e. 3 to 10
				];
			}

		default:
			return state;
	}
}

// reducer that manages single 'todo' component
const todo = (state, action) => {
	switch(action.type){
		case 'ADD_TODO':
			return {
					id: action.id,
					text: action.text,
					completed: false
			}
		
		case 'TOGGLE_TODO':
			if(state.id !== action.id){
				return state;
			}
			return {
				...state,
				completed: !state.completed
			} 		

		default:
			return state;	
	}
}

// visibility reducer
const visibilityFilter = (state='SHOW_ALL', action) => {
	switch(action.type){
		case 'SET_VISIBILITY_FILTER':
			return action.filter;
		default:
			return state;
	}
}

// THIS HAS TO HAPPEN AFTER THE REDUCERS WERE CREATED; PLACE IT ON THE BOTTOM OF THE APP
const { combineReducers } = Redux;

const todoApp = combineReducers({
	todos,
	visibilityFilter
});

const { createStore } = Redux;

const store = createStore(todoApp);


//react provides a base class to create components
const { Component } = React;

//functional component that will show the filtered todos
const FilterLink = ({
	filter, 
	children, 
	currentFilter,
	onClick
}) => {
	// it is possible to make the comparison below beause currentFilter is updated when the action is dispatched
	// once the link is clicked, it calls the 'todoApp' and passes the action
	// the action updates the visibilityFilter
	// visibilityFilter is then passed as prop to the 'FilterLink' component in the ReactDOM.render
	if(filter === currentFilter){
		return <span>{ children }</span>
	}

	return (
		<a href="#"
			onClick={e => {
				e.preventDefault();
				onClick(filter);	
		}}>
			{ children }
		</a>
	);
}


//renders the visibility filters
const Footer = ({
	visibilityFilter,
	onFilterClick
}) => (
	<p className='displayTag'>
		Show:
		{' '} {/*this is just a space*/}
		<FilterLink 
			filter = 'SHOW_ALL'
			currentFilter = {visibilityFilter}
			onClick={onFilterClick}
		>
			All 
		</FilterLink>
		{' '}
		<FilterLink 
			filter = 'SHOW_ACTIVE'
			currentFilter = {visibilityFilter}
			onClick={onFilterClick}
		>
			Active
		</FilterLink>
		{' '}
		<FilterLink 
			filter = 'SHOW_COMPLETED'
			currentFilter = {visibilityFilter}
			onClick={onFilterClick}
		>
			Completed
		</FilterLink>
		{' '}
	</p>
);

// individual todo--this is the component that renders the presentation
// binds its 'onClick' event to 'onClick' passed via props from TodoList 
// onClick event will fire the event from child to parent:
// Todo.onClick triggers TodoList.onClick, which will pass the todo.id via callback and dispatch the event
// takes the 'completed' and 'text' properties of the todos, passed via {...todo} in the loop on TodoList
const Todo = ({
	onClick,
	completed,
	text
}) => (
	<div className='todoWrapper'>
		
		<li onClick={onClick}
			style={{
				cursor: 'pointer',
				marginBottom: '5'
			}}>
		
			{/*<button 
				className='deleteButton'
				onClick={() => {
					store.dispatch({
						type:'DELETE_TODO',
						id: todo.id
					});
				}}>
					x
			</button> */}
				{text}
				<span className={
					completed? 
						'checkTodo' :
						'uncheckTodo'
					}
				>
					√
				</span>
		</li>
		
	</div> 
);

// list of todos
// takes visibleTodos as props and loops over single todo components
// passes all todo objects taken from props to children todo components
// takes function onTodoClick from the instantiation of TodoList in the controller app component (todoApp)
// onTodoClick is a callback and takes the todo.id as argument (argument is passed in the component constructor below, not in the instantiation)
const TodoList = ({
	todos,
	onTodoClick
}) => (
	<ol>
		{todos.map(todo =>
			<Todo 
				key={todo.id}
				{...todo} 
				onClick={()=>onTodoClick(todo.id)} 	/>
			)}
	</ol>
);


// functional component--syntax is different than presentational components
// it has the {} instead of () and has a return statement
const AddTodo = ({
	onAddClick
}) => {
	let input;
	/*the ref API has a callback function; 
		here we get the node and store it in this.input*/
	return (
		
		<div>
			<input type="text" ref={node => {
				input=node;
			}} />
			
			<button onClick={() => {
				if(input.value=='')
				{
					alert('Enter a value');
					return;
				}
				else
				{
					onAddClick(input.value);
				}
				
				input.value='';//clears the input after an action is dispatched
			}}>
				Add Todo
			</button>
		</div>
	);
};


//returns objects depending on their visibility status
const getVisibleTodos =  (todos, filter) => {
	switch(filter){

		//returns all 'todo' objects
		case 'SHOW_ALL':
			return todos;

		// filters 'todo' array and returns only the objects whose 'completed' property is set to false
		case 'SHOW_ACTIVE':
			return todos.filter(
				t => !t.completed
			);

		// filters 'todo' array and returns only the objects whose 'completed' property is set to true
		case 'SHOW_COMPLETED':
			return todos.filter(
				t => t.completed
			);
	}
}



//global variable that stores unique ids
//value is incremented when actions are dispatched (meaning button is clicked)
let nextTodoId=0;

// make this a function instead of a class (like it was on the previous iterations)
// takes the props as arguments
// because we cleaned up and moved getVisibleTodos to where it is being called we have now a single expression
// thus we don't need the return statement anymore
const TodoApp = ({
	todos,
	visibilityFilter
}) => (
	<div>
		<AddTodo 
		onAddClick={text =>
			store.dispatch({
				type: 'ADD_TODO',
				text,
				id: nextTodoId++
			})
		} />

		<TodoList 
			todos={
				getVisibleTodos(
					todos,
					visibilityFilter
				)
			}
			onTodoClick={id =>
				store.dispatch({
					type: 'TOGGLE_TODO',
					id
				})
			}/>

		<Footer 
			visibilityFilter={visibilityFilter}
			onFilterClick={filter => 
					store.dispatch({
						type: 'SET_VISIBILITY_FILTER',
						filter
				})
			}/>
		</div>
);

// the render function will be called on Redux subscribe method (fired every time an action is dispatched)
// it will render the react component with the initial state (we call render() by itself first) 
// and update the state tree every time an event is dispatched (via the subscribe methods)
const render = () =>{
	/*{...store.getState()} pass the whole state tree, meaning all objects whose 
			'SET_VISIBILITY_FILTER' is setvisible*/
	ReactDOM.render(
		<TodoApp 
			{...store.getState()} />, 
		document.getElementById('root')
	);
}

store.subscribe(render);
render();
