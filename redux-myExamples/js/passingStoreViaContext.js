/*
reference: https://facebook.github.io/react/docs/context.html

When not to use context
Just as global variables are best avoided when writing clear code, 
you should avoid using context in most cases. In particular, 
think twice before using it to "save typing" and using it instead of passing explicit props.
The best use cases for context are for implicitly passing down the logged-in user, 
the current language, or theme information. All of these might otherwise be true globals, 
but context lets you scope them to a single React subtree.
Do not use context to pass your model data through components. 
Threading your data through the tree explicitly is much easier to understand. 
Using context makes your components more coupled and less reusable, 
because they behave differently depending on where they're rendered.
*/

// reducer that manages all todos
const todos = (state=[], action) => {
	switch(action.type){
		case 'ADD_TODO':
			return [
				...state,
				todo(undefined, action)
			];

		case 'TOGGLE_TODO':
			return state.map(t=>todo(t, action));
		
		case 'DELETE_TODO':
			console.log('state: ', state);
			console.log('action.id:', action.id);
			// console.log('state.id: ', state[action.id].id);
			
			let todoArrayposition=0;

			state.map((todo, index)=>{
				console.log("index:", index);
				
				if(action.id==todo.id)
				{
					todoArrayposition=index;
					console.log("todoArrayposition:", todoArrayposition);
				}
			});

			if(todoArrayposition==state[0].id || state.length==1) 
			{
				console.log('first element in the array');
				return [
					...state.slice(1, state.length) // we just care about what comes after it, even if it is empty (this happens when state.length==1)
				];
			}
			else
			{
				console.log('Slicing from beginning and end');
				return [
					...state.slice(0, todoArrayposition), // i.e 0 to 2 -- doesn't include 2
					...state.slice(todoArrayposition+1, state.length) //i.e. 3 to 10
				];
			}

		default:
			return state;
	}
}

// reducer that manages single 'todo' component
const todo = (state, action) => {
	switch(action.type){
		case 'ADD_TODO':
			return {
					id: action.id,
					text: action.text,
					completed: false
			}
		
		case 'TOGGLE_TODO':
			if(state.id !== action.id){
				return state;
			}
			return {
				...state,
				completed: !state.completed
			} 		

		default:
			return state;	
	}
}

// visibility reducer
const visibilityFilter = (state='SHOW_ALL', action) => {
	switch(action.type){
		case 'SET_VISIBILITY_FILTER':
			return action.filter;
		default:
			return state;
	}
}

// THIS HAS TO HAPPEN AFTER THE REDUCERS WERE CREATED; PLACE IT ON THE BOTTOM OF THE APP
const { combineReducers } = Redux;

// combine the reducers into one object
// the store is created with the returned result of these objects
const todoApp = combineReducers({
	todos,
	visibilityFilter
});

//react provides a base class to create components
const { Component } = React;

//functional component that will show the filtered todos
const Link = ({
	active,
	children, 
	onClick
}) => {
	// it is possible to make the comparison below beause currentFilter is updated when the action is dispatched
	// once the link is clicked, it calls the 'todoApp' and passes the action
	// the action updates the visibilityFilter
	// visibilityFilter is then passed as prop to the 'FilterLink' component in the ReactDOM.render
	if(active){
		return <span>{ children }</span>
	}

	return (
		<a href="#"
			onClick={e => {
				e.preventDefault();
				onClick();	
		}}>
			{ children }
		</a>
	);
}


//container component that passes data and functionality to the 'Link' presentational component
class FilterLink extends Component {

	/*Below, 'const state' takes the state from Redux state tree
	However, this state is not subscribed to the store, so if the parent doesn't update when the store updates, 
	'const store' below will hold an outdated state
	To fix this we use React's lifecycle methods componentDidMount to subscribe the component to the store and force update of the component when
	an action is dispatched
	forceUpdate forces the re-rendering of the component*/
	componentDidMount() {
		const { store } = this.context;
		this.unsubscribe = store.subscribe(() =>
			this.forceUpdate()
		);
	}

	// here we clean up the action performed on componentDidMount
	// we call the returned value of 'store.subscribe' above, stored in 'this.unsubscribe'
	componentWillUnmount() {
		this.unsubscribe();
	}


	render (){

		const props = this.props; /*takes props from parent Footer--in this case the inly prop is 'filter'*/
		const { store } = this.context;
		const state = store.getState(); /*takes the whole state from Redux state tree*/


		/*active prop compares the filter passed from the link to the visibility state of the that link, taken from the store--returns true or false*/
		/*props.children takes the children from Footer--the text of the links themselves (All, Completed, Active)*/
		return (
			<Link 
				active={props.filter === state.visibilityFilter} 
				onClick={() =>
					store.dispatch({
						type: 'SET_VISIBILITY_FILTER',
						filter: props.filter
					})
				}
			>
			{props.children} 
			</Link>
		);
	}
}

// we also need to specify what context the child will receive
// notice we don't use childcontextTypes as we do on the component that passes the context down to its children
FilterLink.contextTypes = {
	store: React.PropTypes.object
}


//renders the visibility filters
const Footer = () => (
	<p className='displayTag'>
		Show:
		
		{' '} {/*this is just a space*/}
		
		<FilterLink 
			filter = 'SHOW_ALL'
		>
			All 
		</FilterLink>
		
		{' '}

		<FilterLink 
			filter = 'SHOW_ACTIVE'
		>
			Active
		</FilterLink>
		
		{' '}

		<FilterLink 
			filter = 'SHOW_COMPLETED'
		>
			Completed
		</FilterLink>
		
		{' '}
	</p>
);

// individual todo--this is the component that renders the presentation
// binds its 'onClick' event to 'onClick' passed via props from TodoList 
// onClick event will fire the event from child to parent:
// Todo.onClick triggers TodoList.onClick, which will pass the todo.id via callback and dispatch the event
// takes the 'completed' and 'text' properties of the todos, passed via {...todo} in the loop on TodoList
const Todo = ({
	onClick,
	completed,
	text
}) => (
	<div className='todoWrapper'>
		
		<li onClick={onClick}
			style={{
				cursor: 'pointer',
				marginBottom: '5'
			}}>
		
			{/*<button 
				className='deleteButton'
				onClick={() => {
					store.dispatch({
						type:'DELETE_TODO',
						id: todo.id
					});
				}}>
					x
			</button> */}
				{text}
				<span className={
					completed? 
						'checkTodo' :
						'uncheckTodo'
					}
				>
					√
				</span>
		</li>
		
	</div> 
);

// list of todos
// takes visibleTodos as props and loops over single todo components
// passes all todo objects taken from props to children todo components
// takes function onTodoClick from the instantiation of TodoList in the controller app component (todoApp)
// onTodoClick is a callback and takes the todo.id as argument (argument is passed in the component constructor below, not in the instantiation)
const TodoList = ({
	todos,
	onTodoClick
}) => (
	<ol>
		{todos.map(todo =>
			<Todo 
				key={todo.id}
				{...todo} 
				onClick={()=>onTodoClick(todo.id)} 	/>
			)}
	</ol>
);


// functional component--syntax is different than presentational components
// it has the {} instead of () and has a return statement
// here we pass the state as a second argument
const AddTodo = ( props, {store} ) => {
	let input;
	/*the ref API has a callback function; 
		here we get the node and store it in this.input*/
	return (
		
		<div>
			<input type="text" ref={node => {
				input=node;
			}} />
			
			<button onClick={() => {
				if(input.value=='')
				{
					alert('Enter a value');
					return;
				}
				else
				{
					store.dispatch({
						type: 'ADD_TODO',
						text: input.value,
						id: nextTodoId++
					})
				}
				
				input.value='';//clears the input after an action is dispatched
			}}>
				Add Todo
			</button>
		</div>
	);
};

// we also need to specify what context the child will receive
// notice we don't use childcontextTypes as we do on the component that passes the context down to its children
AddTodo.contextTypes = {
	store: React.PropTypes.object
};


//returns objects depending on their visibility status
const getVisibleTodos =  (todos, filter) => {
	switch(filter){

		//returns all 'todo' objects
		case 'SHOW_ALL':
			return todos;

		// filters 'todo' array and returns only the objects whose 'completed' property is set to false
		case 'SHOW_ACTIVE':
			return todos.filter(
				t => !t.completed
			);

		// filters 'todo' array and returns only the objects whose 'completed' property is set to true
		case 'SHOW_COMPLETED':
			return todos.filter(
				t => t.completed
			);
	}
}

// connects the presentation component 'TodoList' to Redux store
// specifies the data and behaviour that the presentation component needs 
class VisibleTodoList extends Component {

	// see comment for the 'why' of componentDidMount and componentWillUnmount on FilterLink class above
	componentDidMount() {
		const { store } = this.context;
		this.unsubscribe = store.subscribe(() =>
			this.forceUpdate()
		);
	}

	componentWillUnmount() {
		this.unsubscribe();
	} 

	render() {
		const props = this.props;
		const { store } = this.context;
		const state = store.getState();

		return (
			<TodoList
				todos={
					getVisibleTodos(
						state.todos,
						state.visibilityFilter
					)
				}
				onTodoClick={id =>
					store.dispatch({
						type: 'TOGGLE_TODO',
						id
					})
				}/>
		);
	}
}

// we also need to specify what context the child will receive
// notice we don't use childcontextTypes as we do on the component that passes the context down to its children
VisibleTodoList.contextTypes = {
	store: React.PropTypes.object
}


//global variable that stores unique ids
//value is incremented when actions are dispatched (meaning button is clicked)
let nextTodoId=0;


const TodoApp = () => (
	<div>
		<AddTodo />

		<VisibleTodoList />

		<Footer />
	</div>
);


// this component will return its children
// so we can wrap any component on this component and it will render the component
class Provider extends Component {
	
	// this is a react native method
	// it makes the 'context' available to all children and grandchildren of the Provider component
	// so, by setting an object with the key 'store' pointing to the value passed down as props in the 
	// instantiation of 'Provider' just below, children and grandchildren have acces to 'this.props.store'
	// via the 'context' key (this.context if the component is a class; if the component is a function,
	// the context is an object passed as second argument, after 'props'. In our code the context is 
	// stored in the 'store' key, so a functional component would receive it like so: (props, ({ store }) ))
	// reference: https://facebook.github.io/react/docs/context.html
	// reference: https://www.tildedave.com/2014/11/15/introduction-to-contexts-in-react-js.html
	getChildContext() {
		return {
			store: this.props.store
		};
	}

	render() {
		return this.props.children;
	}
}

// to enable children to receive the context we need to specify the type that is being passed
// reference: https://facebook.github.io/react/docs/context.html
// reference: https://www.tildedave.com/2014/11/15/introduction-to-contexts-in-react-js.html
Provider.childContextTypes = {
	store: React.PropTypes.object
};

const { createStore } = Redux;

//we will now pass the store directly as props


// because the components are now subscribed to the store, they can keep track of the state themselves
// we don't need to pass the ...store.getState() as props from the parent anymore
// we also don't need the render function anymore because we will render the app once and then each component keeps track of their state themselves
// here, <VisibleTodoList /> and <Footer /> (via FilterLink) will keep track of their state; <AddTodo /> doesn't need to know the state of the app
// we don't need to re-render the whole app every time an action is dispatched
// we now re-render only the component that corresponds to the action dispatched
ReactDOM.render(
	<Provider store={createStore(todoApp)}>
		<TodoApp />
	</Provider>
	, 
	document.getElementById('root')
);