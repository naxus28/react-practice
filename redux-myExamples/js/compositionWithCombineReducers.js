
//reducer that manages all todos
const todos = (state=[], action) => {
	switch(action.type){
		case 'ADD_TODO':
			return [
				...state,
				todo(undefined, action)
			];
			break;

		case 'TOGGLE_TODO':
			return state.map(t=>todo(t, action));
			break;
		
		default:
			return state;
	}
}

//reducer that manages single 'todo' component
const todo = (state, action) => {
	switch(action.type){
		case 'ADD_TODO':
			return {
					id: action.id,
					text: action.text,
					completed: false
			}
			break;
		
		case 'TOGGLE_TODO':
			if(state.id !== action.id){
				return state;
			}
			return {
				...state,
				completed: !state.completed
			}

		default:
			state;	
	}
}

//visibility reducer
const visibilityFilter = (state='SHOW_ALL', action) => {
	switch(action.type){
		case 'SET_VISIBILITY_FILTER':
			return action.filter;
		default:
			return state;
	}
}

//main reducer--controller
// const todoApp = (state = {}, action) => {
// 	return {
// 		todos: todos(
// 			state.todos,
// 			action
// 		),
// 		visibilityFilter: visibilityFilter(
// 			state.visibilityFilter,
// 			action)
// 	}
// }

//replace the hand written function above for a top level reducer function: combineReducers
//like the function above, combineReducers returns a combination of the reducers in the app

//THIS HAS TO HAPPEN AFTER THE REDUCERS WERE CREATED; PLACE IT ON THE BOTTOM OF THE APP
const { combineReducers } = Redux;

//since our key and value have the same anme, we can ommit the value and just use the key (due to ES6 object shorthand notation)
// const todoApp = combineReducers({
// 	todos: todos,
// 	visibilityFilter: visibilityFilter
// });

const todoApp = combineReducers({
	todos,
	visibilityFilter
});

const { createStore } = Redux;

const store = createStore(todoApp);


//*************
console.log('Current State');
console.log(store.getState());
console.log('---------------');

//*************
console.log('Dispatching ADD_TODO');
store.dispatch({
		id: 0,
		type: 'ADD_TODO',
		text: 'Learn Redux'
	});
console.log(store.getState());
console.log('---------------');

//*************
console.log('Dispatching ADD_TODO');
store.dispatch({
		id: 1,
		type: 'ADD_TODO',
		text: 'Go Shopping'
	});
console.log(store.getState());
console.log('---------------');

//*************
console.log('Dispatching TOGGLE_TODO');
store.dispatch({
		id: 0,
		type: 'TOGGLE_TODO',
	});
console.log(store.getState());
console.log('---------------');

//*************
console.log('Dispatching SET_VISIBILITY_FILTER');
store.dispatch({
		type: 'SET_VISIBILITY_FILTER',
		filter: 'SHOW_COMPLETED'
	});
console.log('getting state');
console.log(store.getState());
console.log('---------------');





//*************TESTS*************

//test function
const testTodo = () => {
	//initial state
	const stateBefore = [];

	//action to be passed
	const action = {
		id: 0,
		type: 'ADD_TODO',
		text: 'Learn Redux'
	}

	//what we expect as new state (not a mutation of initial state, but a new object altogether)
	const stateAfter = [
		{
			id: 0,
			text: 'Learn Redux',
			completed: false
		}
	];

	deepFreeze(stateBefore);
	deepFreeze(action);
	
	//by passing "stateBefore" and "action" into todos function we expect the result to equal "stateAfter"
	expect(
		todos(stateBefore, action)
	).toEqual(stateAfter);
}

//test function
const testToggleTodo = () => {
	//initial state
	const stateBefore = [
		{
			id: 0,
			text: 'Learn Redux',
			completed: false
		},
		{
			id: 1,
			text: 'Go Shopping',
			completed: false
		}
	];

	//action to be passed
	const action = {
		id: 1,
		type: 'TOGGLE_TODO',
	}

	//array we expect as new state (not a mutation of initial state, but a new object altogether)
	const stateAfter = [
		{
			id: 0,
			text: 'Learn Redux',
			completed: false
		},
		{
			id: 1,
			text: 'Go Shopping',
			completed: true
		}
	];

	//freeze the state and the action to make sure we don't mutate them accidentally
	deepFreeze(stateBefore);
	deepFreeze(action);

	//by passing "stateBefore" and "action" into the 'todos' function we expect the result to equal "stateAfter"
	expect(
		todos(stateBefore, action)
	).toEqual(stateAfter);
}

testTodo();
testToggleTodo();
console.log('Tests passed');