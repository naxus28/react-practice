// reducer takes the state and action
const todos = (state=[], action) => {
	switch(action.type){
		// returns the new state of switch case is a match
		//new state is the old state (empty array) + the object constructed in the return statement
		//new state will then be a single object
		case 'ADD_TODO':
			return [
				...state,
				{
					id: action.id,
					text: action.text,
					completed: false
				}
			];

		case 'TOGGLE_TODO':
			//map function returns a new array--we can't mutate the state
			return state.map(todo => {
				//if the object's state.id is different than the action.id
				//just return the object
				if(todo.id !== action.id){
					return todo;
				}


				//if the object's state.id is the same as the action.id
				//return the object and flip the 'completed' key's value
				return {
					...todo,
					completed: !todo.completed
				}
			});
			
		default:
			return state;
	}
}

//test function
const testTodo = () => {
	//initial state
	const stateBefore = [];

	//action to be passed
	const action = {
		id: 0,
		type: 'ADD_TODO',
		text: 'Learn Redux'
	}

	//what we expect as new state (not a mutation of initial state, but a new object altogether)
	const stateAfter = [
		{
			id: 0,
			text: 'Learn Redux',
			completed: false
		}
	];

	deepFreeze(stateBefore);
	deepFreeze(action);
	
	//by passing "stateBefore" and "action" into todos function we expect the result to equal "stateAfter"
	expect(
		todos(stateBefore, action)
	).toEqual(stateAfter);
}

//test function
const testToggleTodo = () => {
	//initial state
	const stateBefore = [
		{
			id: 0,
			text: 'Learn Redux',
			completed: false
		},
		{
			id: 1,
			text: 'Go Shopping',
			completed: false
		}
	];

	//action to be passed
	const action = {
		id: 1,
		type: 'TOGGLE_TODO',
	}

	//array we expect as new state (not a mutation of initial state, but a new object altogether)
	const stateAfter = [
		{
			id: 0,
			text: 'Learn Redux',
			completed: false
		},
		{
			id: 1,
			text: 'Go Shopping',
			completed: true
		}
	];

	//freeze the state and the action to make sure we don't mutate them accidentally
	deepFreeze(stateBefore);
	deepFreeze(action);

	//by passing "stateBefore" and "action" into the 'todos' function we expect the result to equal "stateAfter"
	expect(
		todos(stateBefore, action)
	).toEqual(stateAfter);
}

testTodo();
testToggleTodo();
console.log('Test passed');