//reducer function
//This doesn't work on Chrome because it has problems with default arguments (state=0)
const counter = (state=0, action) => {
	switch (action.type)
	{
		case 'INCREMENT':
			return state+1;
		case 'DECREMENT':
			return state-1;
		default:
			return state;
	}
}

//createStore is a method from the Redux library
//binds the 3 principles of Redux: Single source of truth; State is read-only; Mutations are written as pure functions

/*3 methods:
	getState()--returns current state
	dispatch()--dispatches an action to the store that will likely update the state
	subscribe()--fires every time an action is dispatched
*/

const { createStore } = Redux;

//passes the reducer into the store, which will allow to tell the reducer how it will update the state via actions
const store = createStore(counter);

//get the state
console.log(store.getState());//0


//dispatch an action
store.dispatch({type: 'INCREMENT'});
console.log(store.getState());//1

//dispatch an action
store.dispatch({type: 'DECREMENT'});
console.log(store.getState());//0

//render the state to the document.body
const render = () => {
	document.body.innerHTML = store.getState();
}

//registers a call back that will be called anytime an action has been dispatched
store.subscribe(render);

//puts the initial state on the screen before we start updating it
render();

//dispactches events on click
addEventListener('click', () => {
	store.dispatch({type: 'INCREMENT'});
});