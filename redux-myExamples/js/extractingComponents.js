
// reducer that manages all todos
const todos = (state=[], action) => {
	switch(action.type){
		case 'ADD_TODO':
			return [
				...state,
				todo(undefined, action)
			];

		case 'TOGGLE_TODO':
			return state.map(t=>todo(t, action));
		
		case 'DELETE_TODO':
			console.log('state: ', state);
			console.log('action.id:', action.id);
			// console.log('state.id: ', state[action.id].id);
			
			let todoArrayposition=0;

			state.map((todo, index)=>{
				console.log("index:", index);
				
				if(action.id==todo.id)
				{
					todoArrayposition=index;
					console.log("todoArrayposition:", todoArrayposition);
				}
			});

			if(todoArrayposition==state[0].id || state.length==1) 
			{
				console.log('first element in the array');
				return [
					...state.slice(1, state.length) // we just care about what comes after it, even if it is empty (this happens when state.length==1)
				];
			}
			else
			{
				console.log('Slicing from beginning and end');
				return [
					...state.slice(0, todoArrayposition), // i.e 0 to 2 -- doesn't include 2
					...state.slice(todoArrayposition+1, state.length) //i.e. 3 to 10
				];
			}

		default:
			return state;
	}
}

// reducer that manages single 'todo' component
const todo = (state, action) => {
	switch(action.type){
		case 'ADD_TODO':
			return {
					id: action.id,
					text: action.text,
					completed: false
			}
		
		case 'TOGGLE_TODO':
			if(state.id !== action.id){
				return state;
			}
			return {
				...state,
				completed: !state.completed
			} 		

		default:
			return state;	
	}
}

// visibility reducer
const visibilityFilter = (state='SHOW_ALL', action) => {
	switch(action.type){
		case 'SET_VISIBILITY_FILTER':
			return action.filter;
		default:
			return state;
	}
}

// THIS HAS TO HAPPEN AFTER THE REDUCERS WERE CREATED; PLACE IT ON THE BOTTOM OF THE APP
const { combineReducers } = Redux;

const todoApp = combineReducers({
	todos,
	visibilityFilter
});

const { createStore } = Redux;

const store = createStore(todoApp);


//react provides a base class to create components
const { Component } = React;

//functional component that will show the filtered todos
const FilterLink = ({filter, children, currentFilter}) => {
	// it is possible to make the comparison below beause currentFilter is updated when the action is dispatched
	// once the link is clicked, it calls the 'todoApp' and passes the action
	// the action updates the visibilityFilter
	// visibilityFilter is then passed as prop to the 'FilterLink' component in the ReactDOM.render
	if(filter === currentFilter){
		return <span>{ children }</span>
	}

	return (
		<a href="#"
			onClick={e => {
				e.preventDefault();
				store.dispatch({
					type: 'SET_VISIBILITY_FILTER',
					filter
			});
		}}>{ children }</a>
	);
}

//returns objects depending on their visibility status
const getVisibleTodos =  (todos, filter) => {
	switch(filter){

		//returns all 'todo' objects
		case 'SHOW_ALL':
			return todos;

		// filters 'todo' array and returns only the objects whose 'completed' property is set to false
		case 'SHOW_ACTIVE':
			return todos.filter(
				t => !t.completed
			);

		// filters 'todo' array and returns only the objects whose 'completed' property is set to true
		case 'SHOW_COMPLETED':
			return todos.filter(
				t => t.completed
			);
	}
}



//global variable that stores unique ids
//value is incremented when actions are dispatched (meaning button is clicked)
let nextTodoId=0;

//custom component that extends the react component
class TodoApp extends Component{
	render() {
		const {
			todos,
			visibilityFilter
		} = this.props;

		// returns the objects according to the filter passed
		// i.e. if there are 4 items on the list and we apply 'SHOW_ALL' it returns 4 objects
		// it we toggle 2 items and apply filter 'SHOW ACTIVE', it returns 2 objects, etc
		const visibleTodos = getVisibleTodos(
			todos,
			visibilityFilter
		);
		// console.log("visibleTodos: ", visibleTodos);
		
		return(
			<div>
				{/*the ref API has a callback function; 
				here we get the node and store it in this.input*/}
				<input type="text" ref={node => {
					this.input=node;
				}} />
				

				<button onClick={() => {
					if(this.input.value=='')
					{
						alert('Enter a value');
						return;
					}
					else
					{
						store.dispatch({
							type: 'ADD_TODO',
							text: this.input.value,//grabs the value of the input
							id: nextTodoId++
						});
					}
					
					this.input.value='';//clears the input after an action is dispatched
				}}>
					Add Todo
				</button>
				<ol>
					{/*loops over the objects returned from 'getVisibleTodos'*/}
					{visibleTodos.map(todo =>  
						<div className='todoWrapper' 
							key={todo.id}>
							
							<li onClick={() => {
									store.dispatch({
										type: 'TOGGLE_TODO',
										id: todo.id
									});
								}}
								style={{
									cursor: 'pointer',
									marginBottom: '5'
								}}>
							
								<button 
									className='deleteButton'
									onClick={() => {
										store.dispatch({
											type:'DELETE_TODO',
											id: todo.id
										});
									}}>
										x
								</button> 
									{todo.text}

								<span className={
									todo.completed? 
										'checkTodo' :
										'uncheckTodo'
									}
								>
									√
								</span>
							</li>
						</div> 
					)}
				</ol>
				<p className='displayTag'>
					Show:
					{' '} {/*this is just a space*/}
					<FilterLink 
						filter = 'SHOW_ALL'
						currentFilter = {visibilityFilter}
					>
						All 
					</FilterLink>
					{' '}
					<FilterLink 
						filter = 'SHOW_ACTIVE'
						currentFilter = {visibilityFilter}
					>
						Active
					</FilterLink>
					{' '}
					<FilterLink 
						filter = 'SHOW_COMPLETED'
						currentFilter = {visibilityFilter}
					>
						Completed
					</FilterLink>
					{' '}
				</p>
			</div>
		);
	}
}

// the render function will be called on Redux subscribe method (fired every time an action is dispatched)
// it will render the react component with the initial state (we call render() by itself first) 
// and update the state tree every time an event is dispatched (via the subscribe methods)
const render = () =>{
	/*{...store.getState()} pass the whole state tree, meaning all objects whose 
			'SET_VISIBILITY_FILTER' is setvisible*/
	ReactDOM.render(
		<TodoApp 
			{...store.getState()} />, 
		document.getElementById('root')
	);
}

store.subscribe(render);
render();
