const toggleTodo = (todo) => {
	//test 1: will fail if we use deepFreeze
	//mutates the state
	/*todo.completed = !todo.completed;
	return todo;*/

	//Object.assign creates a copy of the old obj; it doesnt mutate the old obj
	// Object.assign(target, ...sources); it also allows to mutate the new object by passing a new value as the 3rd argument
	//reference: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
	// return Object.assign({}, todo, {
	// 	completed: !todo.completed
	// });
	

	//object spread needs babel stage 2 plugin; installed as node_module
	//node_module/babel-plugin-syntax-object-rest-spread
	//references: https://www.npmjs.com/package/babel-plugin-transform-object-rest-spread
	//https://babeljs.io/docs/plugins/syntax-object-rest-spread/
	return {
		 ...todo,
		 completed: !todo.completed
	}
}

const testToggleTodo = () => {
	const todoBefore = {
		id: 0,
		text: 'Learn Redux',
		completed: false
	}

	const todoAfter = {
		id: 0,
		text: 'Learn Redux',
		completed: true
	}

	deepFreeze(todoBefore);

	expect(
		toggleTodo(todoBefore)
		).toEqual(todoAfter);
}

testToggleTodo();
console.log("Tests passed");