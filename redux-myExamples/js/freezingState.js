//function that appends element to array
const addCounter = (list) => {
	// *****TEST 1*****
	// test fails
	// mutates the state of the array; not possible if we use deepFreeze; 
	// list.push(0);
	// return list;

	// *****TEST 2*****
	// test passes
	// doesn't mutate the state of the array
	// return list.concat([0]);

	// *****TEST 3*****
	// ES6 syntax; array spread operator: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_operator
	return [...list, 0, 1, 2];
}

//function that removes element from array
const removeCounter = (list, index) => {
	// *****TEST 1*****
	// test fails
	// mutates the state of the array; not possible if we use deepFreeze; 
	// list.splice(index,1);
	// return list;

	// *****TEST 2*****
	// test passes
	// return list
			// .slice(0, index)//returns the part of the array from index 0 to whatever the 'index' is (in this case, we passed 1 from inside testRemoveCounter, so the number sliced is 10)
			// .concat(list.slice(index+1));//slice the element that comes right after the 'index' (in our case, 30) and concat to the array
			//this function returns a new array. It doesn't mutate the state of the previous array

	//*****TEST 3*****
	//ES6 Array spread operator
	//appends two items to the array
	return [
		...list.slice(0, index), 
		...list.slice(index+1)
	];

}

//function that increments counter
const incrementCounter = (list, index) => {
	// *****TEST 1*****
	// test fails
	// mutates the state of the array; not possible if we use deepFreeze;
	//list[index]++;
	// return list;

	// *****TEST 2*****
	//test passes
	// return list
	// 		.slice(0, index)
	// 		.concat([list[index]+1])
	// 		.concat(list.slice(index+1))

	// *****TEST 3*****
	//ES6 Array spread operator
	return [
		...list.slice(0, index),
		list[index]+1,
		...list.slice(index+1)
	];
}

//tests adding results
const testAddCounter = () => {
	const listBefore = [];
	const listAfter = [0, 1, 2];

	//freezes the state of the array; makes mutation not possible
	deepFreeze(listBefore);

	//test: 'listBefore' is expected to be  equal to 'listAfter' after we pass it to addCounter()
	expect(
		addCounter(listBefore)
	).toEqual(listAfter);
}

//tests removing results
const testRemoveCounter = () => {
	const listBefore = [10, 20, 30];
	const listAfter = [10, 30];

	deepFreeze(listBefore);

	expect(
		removeCounter(listBefore, 1)
	).toEqual(listAfter);
}

const testIncrementCounter = () => {
	const listBefore = [0, 10, 20];
	const listAfter = [0, 11, 20];

	expect(
		incrementCounter(listBefore, 1)
	).toEqual(listAfter);
}

testAddCounter();
testRemoveCounter();
testIncrementCounter();

console.log('test passed');