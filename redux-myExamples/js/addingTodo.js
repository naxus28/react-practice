
// reducer that manages all todos
const todos = (state=[], action) => {
	switch(action.type){
		case 'ADD_TODO':
			return [
				...state,
				todo(undefined, action)
			];

		case 'TOGGLE_TODO':
			return state.map(t=>todo(t, action));

		default:
			return state;
	}
}

// reducer that manages single 'todo' component
const todo = (state, action) => {
	switch(action.type){
		case 'ADD_TODO':
			return {
					id: action.id,
					text: action.text,
					completed: false
			}
		
		case 'TOGGLE_TODO':
			if(state.id !== action.id){
				return state;
			}
			return {
				...state,
				completed: !state.completed
			} 		

		default:
			return state;	
	}
}

// visibility reducer
const visibilityFilter = (state='SHOW_ALL', action) => {
	switch(action.type){
		case 'SET_VISIBILITY_FILTER':
			return action.filter;
		default:
			return state;
	}
}

// THIS HAS TO HAPPEN AFTER THE REDUCERS WERE CREATED; PLACE IT ON THE BOTTOM OF THE APP
const { combineReducers } = Redux;

const todoApp = combineReducers({
	todos,
	visibilityFilter
});

const { createStore } = Redux;

const store = createStore(todoApp);


//react provides a base class to create components
const { Component } = React;

//global variable that stores unique ids
//value is incremented when actions are dispatched (meaning button is clicked)
let nextTodoId=0;

//custom component that extends the react component
class TodoApp extends Component{
	render() {
		return(
			<div>
				{/*the ref API has a callback function; 
				here we get the node and store it in this.input*/}
				<input type="text" ref={node=>{
					this.input=node;
				}} />
				<button onClick={()=>{
					store.dispatch({
						type: 'ADD_TODO',
						text: this.input.value,//grabs the value of the input
						id: nextTodoId++
					});
					this.input.value='';//clears the input after an action is dispatched
				}}>
					Add Todo
				</button>
				<ul>
					{this.props.todos.map(todo =>  
						<li key={todo.id}>
							{todo.text}
						</li>
					)}
				</ul>
			</div>
		);
	}
}

// the render function will be called on Redux subscribe method (fired every time an action is dispatched)
// it will render the react component with the initial state (we call render() by itself first) 
// and update the state tree every time an event is dispatched (via the subscribe methods)
const render = () =>{
	ReactDOM.render(
		<TodoApp 
			todos={store.getState().todos} />,
		document.getElementById('root')
	);
}

store.subscribe(render);
render();


/*
//*************
console.log('Current State');
console.log(store.getState());
console.log('---------------');

//*************
console.log('Dispatching ADD_TODO');
store.dispatch({
		id: 0,
		type: 'ADD_TODO',
		text: 'Learn Redux'
	});
console.log(store.getState());
console.log('---------------');

//*************
console.log('Dispatching ADD_TODO');
store.dispatch({
		id: 1,
		type: 'ADD_TODO',
		text: 'Go Shopping'
	});
console.log(store.getState());
console.log('---------------');

//*************
console.log('Dispatching TOGGLE_TODO');
store.dispatch({
		id: 0,
		type: 'TOGGLE_TODO',
	});
console.log(store.getState());
console.log('---------------');

//*************
console.log('Dispatching SET_VISIBILITY_FILTER');
store.dispatch({
		type: 'SET_VISIBILITY_FILTER',
		filter: 'SHOW_COMPLETED'
	});
console.log('getting state');
console.log(store.getState());
console.log('---------------');


//*************
console.log('Dispatching DELETE_TODO');
store.dispatch({
		id: 1,//this is the element we want to delete (not the index)
		type: 'DELETE_TODO',
	});
console.log(store.getState());
console.log('---------------');

//*************
console.log('Dispatching DELETE_TODO');
store.dispatch({
		id: 1,//this is the element we want to delete (not the index)
		type: 'DELETE_TODO',
	});
console.log(store.getState());
console.log('---------------');


*/


//*************TESTS*************

//test function
const testTodo = () => {
	//initial state
	const stateBefore = [];

	//action to be passed
	const action = {
		id: 0,
		type: 'ADD_TODO',
		text: 'Learn Redux'
	}

	//what we expect as new state (not a mutation of initial state, but a new object altogether)
	const stateAfter = [
		{
			id: 0,
			text: 'Learn Redux',
			completed: false
		}
	];

	deepFreeze(stateBefore);
	deepFreeze(action);
	
	//by passing "stateBefore" and "action" into todos function we expect the result to equal "stateAfter"
	expect(
		todos(stateBefore, action)
	).toEqual(stateAfter);
}

//test function
const testToggleTodo = () => {
	//initial state
	const stateBefore = [
		{
			id: 0,
			text: 'Learn Redux',
			completed: false
		},
		{
			id: 1,
			text: 'Go Shopping',
			completed: false
		}
	];

	//action to be passed
	const action = {
		id: 1,
		type: 'TOGGLE_TODO',
	}

	//array we expect as new state (not a mutation of initial state, but a new object altogether)
	const stateAfter = [
		{
			id: 0,
			text: 'Learn Redux',
			completed: false
		},
		{
			id: 1,
			text: 'Go Shopping',
			completed: true
		}
	];

	//freeze the state and the action to make sure we don't mutate them accidentally
	deepFreeze(stateBefore);
	deepFreeze(action);

	//by passing "stateBefore" and "action" into the 'todos' function we expect the result to equal "stateAfter"
	expect(
		todos(stateBefore, action)
	).toEqual(stateAfter);
}

testTodo();
testToggleTodo();
console.log('Tests passed');